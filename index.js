require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose');
const app = express()
const port = 3000
const path = require('path')
const bookRoutes = require('./routes/bookRoutes');
const authorRoutes = require('./routes/authorRoutes');
const userRoutes = require('./routes/userRoutes')
const authenticationRoutes = require('./routes/authenticationRoutes')
const cors =require('cors')


app.use(cors({
  credentials:true,
  origin:true
}))
app.use(express.json())
app.use('/books',bookRoutes)
app.use('/authors',authorRoutes)
app.use('/users', userRoutes)
app.use('/auth', authenticationRoutes)
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

main().catch(err => console.log(err));
async function main() {
  await mongoose.connect(process.env.DB_URL);
  console.log('database connected')
}