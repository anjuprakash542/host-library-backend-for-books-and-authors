const express = require('express');
const Book = require("../models/bookModels")

const getAllBooks = async (req,res) => {
    try { 
        const reqQuery ={...req.query}
        const fieldsToDelete = ['limit','sort','select']
        fieldsToDelete.forEach(field =>{
          delete reqQuery [field]
        })
        console.log(reqQuery)
        const query = Book.find(reqQuery)
      
        if(req.query.sort) {
          query.sort(req.query.sort)
        }
      
        if (req.query.select) {
          let selectString =req.query.select.replace(/,/g," ");
          query.select(selectString)
          console.log(req.query.select)
          console.log(selectString)
        }
        if(req.query.limit){
          query.limit(req.query.limit)
        }
        const book =await query.exec()
        res.status(200).json(book)
 }
 catch(error) {
  console.log(error)
 }
  

}

const getBookById = async (req,res) => {
    try {
        const books = await Book.findById(req.params.bookId).exec();
    res.status(200).json(books)
    }
    catch(error) {
        res.status(404).send('Book not found')
    }
}

const addNewBook = async (req,res) => {
  const bookData = req.body;
  const book = new Book (bookData)
  await book.save()
  res.status(201).json(book)
}
const updateBook =async (req,res) => {
   try { 
    const books = await Book.findByIdAndUpdate(req.params.bookId,req.body,{new:true});
      res.json(books)
   }
   catch(error){
    console.log(error)
    res.status(404).send('The book you want to update is not found')
   

   }
}

const deleteBook =async (req,res) => {
    try {
        await Book.findByIdAndDelete(req.params.bookId)
        res.status(200).send('deleted')

    }
    catch(error){
        res.status(404).send('The book that you want to delete is not found')
    }
}

module.exports ={
    getAllBooks,
    getBookById,
    addNewBook,
    updateBook,
    deleteBook
}